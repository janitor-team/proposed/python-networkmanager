python-networkmanager (2.2-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andreas Henriksson ]
  * New upstream release.
    - includes compatibility additions with newer NM (Closes: #984983)
  * Refresh offsets in d/p/0003-Gracefully-handle-dbus-exception.patch
  * Update years in debian/copyright to match upstream

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 13 Mar 2021 19:29:10 +0100

python-networkmanager (2.1-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 21:17:30 +0200

python-networkmanager (2.1-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ W. Martin Borgert ]
  * New upstream version (Closes: #907645).
  * Add patch to gracefully handle dbus exception,
    e.g. for testing in chroot (Closes: #896281).
  * Remove old patches, not needed for new version.

 -- W. Martin Borgert <debacle@debian.org>  Fri, 31 Aug 2018 09:23:55 +0000

python-networkmanager (2.0.1-4) unstable; urgency=medium

  * Team upload.
  * Add some upstream changes for more robust behavior.

 -- David Steele <steele@debian.org>  Fri, 23 Feb 2018 22:11:19 -0500

python-networkmanager (2.0.1-2) unstable; urgency=medium

  * Team upload.

  [ W. Martin Borgert <debacle@debian.org> 2018-02-06 ]
  * Reflect license change in debian/copyright (Closes: #889034).
  * Fix Python 2 package by using pybuild (Closes: #889039).

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ David Steele ]
  * Update Debian Standards to v4.1.3.

 -- David Steele <steele@debian.org>  Mon, 19 Feb 2018 22:34:06 -0500

python-networkmanager (2.0.1-1) unstable; urgency=medium

  * New upstream release (Closes: #860727)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 11 Jan 2018 22:17:33 +0100

python-networkmanager (0.9.10-2) unstable; urgency=medium

  * Team upload.
  * Add a python3 version of the package (Closes: #777516).
  * Address some lintian issues.

 -- David Steele <steele@debian.org>  Wed, 10 Jan 2018 20:19:36 -0500

python-networkmanager (0.9.10-1) unstable; urgency=low

  * Initial release (Closes: #702587)

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 13 Sep 2013 12:09:03 -0400
