Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-networkmanager
Upstream-Contact: Dennis Kaarsemaker <dennis@kaarsemaker.net>
Source: https://github.com/seveas/python-networkmanager

Files: *
Copyright: 2011-2021 Dennis Kaarsemaker <dennis@kaarsemaker.net>
License: zlib

Files: debian/*
Copyright: 2013, Hans-Christoph Steiner <hans@eds.org>
           2011,2012 Dennis Kaarsemaker <dennis@kaarsemaker.net>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3 of the License, or (at your
 option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
Comment:
 On Debian systems the 'GNU General Public License' version 3 is located
 in '/usr/share/common-licenses/GPL-3'.

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgement in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
